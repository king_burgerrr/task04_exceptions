package com.veselskyi.exception;

public class UserNotFoundException extends UserException {

    public UserNotFoundException(String message) {
        super(message);
    }

}
