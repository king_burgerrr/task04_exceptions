package com.veselskyi.exception;

public class ValidationException extends Exception {

    public ValidationException(String message) {
        super(message);
    }

}
