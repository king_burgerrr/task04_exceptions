package com.veselskyi.controller;

import com.veselskyi.exception.ValidationException;
import com.veselskyi.model.Resource;
import com.veselskyi.model.Server;
import com.veselskyi.model.User;

import java.util.List;

public class Controller {

    private Server server = new Server();

    public Controller() {
    }

    public void register(User user) throws ValidationException {
        server.register(user);
    }

    public void authorize(User user) {
        server.authorize(user);
    }

    public List<User> getAuthorizedUsers() {
        return server.getAuthorizedUsers();
    }

    public Resource getFileResource(User user, String fileName) {
        return server.getFileResource(user, fileName);
    }

}
