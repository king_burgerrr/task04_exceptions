package com.veselskyi.view;

import com.veselskyi.controller.Controller;
import com.veselskyi.exception.ResourceException;
import com.veselskyi.exception.ValidationException;
import com.veselskyi.model.Resource;
import com.veselskyi.model.User;

public class ConsoleView {

    private Controller controller = new Controller();

    public void show() throws ValidationException, ResourceException {
        User admin = new User("admin", "adminadmin");
        controller.register(admin);
        controller.authorize(admin);

        Resource fileResource = controller.getFileResource(admin, "test.txt");
        fileResource.clear();

        fileResource.write("Test string", true);
        System.out.println(fileResource.read());

        try (Resource fr = controller.getFileResource(admin, "test.txt")) {
            fr.write("Hello world");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
