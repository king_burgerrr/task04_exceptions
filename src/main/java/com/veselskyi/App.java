package com.veselskyi;

import com.veselskyi.exception.ResourceException;
import com.veselskyi.exception.ValidationException;
import com.veselskyi.view.ConsoleView;

public class App {
    public static void main(String[] args) throws ValidationException, ResourceException {
        (new ConsoleView()).show();
    }
}
