package com.veselskyi.service;

import com.veselskyi.model.User;
import com.veselskyi.model.UserRepo;

public class UserService {

    private UserRepo userRepo;

    public UserService(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    public void addUser(User user) {
        userRepo.addUser(user);
    }

    public User getUserByUsername(final String username) {
        return userRepo.getUserByUsername(username);
    }

}
