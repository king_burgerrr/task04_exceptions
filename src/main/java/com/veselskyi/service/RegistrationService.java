package com.veselskyi.service;

import com.veselskyi.exception.UserExistsException;
import com.veselskyi.exception.ValidationException;
import com.veselskyi.model.User;
import com.veselskyi.model.Validator;

public class RegistrationService {

    private UserService userService;
    private Validator validator;

    private RegistrationService() {
        validator = new Validator();
    }

    public RegistrationService(UserService userService) {
        this();
        this.userService = userService;
    }

    public boolean register(User user) throws ValidationException {
        if (validator.validate(user)) {
            if (userService.getUserByUsername(user.getUsername()) != null) {
                return false;
            }
            userService.addUser(user);
        }
        return true;
    }

}
